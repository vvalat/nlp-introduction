#! /bin/bash

PORT=9004
TIMEOUT=15000

if ! hash java 2>/dev/null; then
    echo "Java is not installed"
    exit 1
fi

printf "Which core would you like to run?\n0: English\n1: French\n"

read -r option

cd stanford-corenlp-full-2018-02-27

# Other preload options (increases processing): lemma, ner, depparse

if [ "$option" == "0" ]; then
    java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer \
    -preload tokenize,ssplit,pos,parse \
    -status_port "$PORT" -port "$PORT" -timeout "$TIMEOUT"
elif [ "$option" == "1" ]; then
    java -Xmx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer \
    -serverProperties StanfordCoreNLP-french.properties \
    -preload tokenize,ssplit,pos,parse \
    -status_port "$PORT"  -port "$PORT" -timeout "$TIMEOUT"
else
    echo "Invalid choice"
    exit 1
fi
