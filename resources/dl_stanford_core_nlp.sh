#! /bin/bash

echo "Input \`fr\` for the extra french package, otherwise just hit the \`Enter\` key"

read -r option

if [ -d "stanford-corenlp-full-2018-02-27" ]; then
    echo "Installation detected, delete \`stanford-corenlp-full-2018-02-27\` directory to install again"
else
    wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-02-27.zip
    unzip stanford-corenlp-full-2018-02-27.zip
fi

if [ "$option" == "fr" ]; then
    cd stanford-corenlp-full-2018-02-27 || exit 1
    if [ -e stanford-french-corenlp-2018-02-27-models.jar ] && [ -e StanfordCoreNLP-french.properties ]; then
        echo "French package already present"
        cd ..
        exit 0
    fi
    wget http://nlp.stanford.edu/software/stanford-french-corenlp-2018-02-27-models.jar
    wget https://raw.githubusercontent.com/stanfordnlp/CoreNLP/master/src/edu/stanford/nlp/pipeline/StanfordCoreNLP-french.properties
    cd ..
fi
