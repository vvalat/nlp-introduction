import os

import requests

from typing import List

API_ENDPOINT = "https://api.twitter.com/2/tweets/search/recent"


def load_token_from_file() -> str:
    """
    Let's avoid committing tokens on public repos
    :return: the token
    """
    file_path = os.path.join(os.path.dirname(__file__), "twitter_token")
    if not os.path.isfile(file_path):
        raise Exception("Missing twitter token file")
    with open(file_path, "r") as file:
        return file.read()


def search_tweets(query: str,
                  max_results: int = 20,
                  lang: str = None,
                  verified: bool = False,
                  retweet: bool = False) -> requests.Response:
    """
    Calls Twitter V2 search endpoint with provided arguments
    :param query: the search string
        -> https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-rule
    :param max_results: maximum number of results to fetch (limited to 100 per request)
    :param lang: BCP 47 identifier (see above link for possible values)
    :param verified: Whether the retrieved tweets comes from a verified user or not
    :param retweet: Whether the results should include retweets or not
    :return: the response from twitter API
    """
    headers = {"Authorization": f"Bearer {load_token_from_file()}", "Accept": "application/json"}
    params = {"query": query, "max_results": max_results}
    if lang is not None:
        params["query"] += f" lang:{lang}"
    if verified:
        params["query"] += " is:verified"
    if not retweet:
        params["query"] += " -is:retweet"
    return requests.get(API_ENDPOINT, params=params, headers=headers)


def get_tweets_list(data: dict,
                    emojis: bool = False,
                    urls: bool = False,
                    hashtags: bool = False,
                    mentions: bool = False) -> List[str]:
    """
    :param data: tweets in a JSON translated format
    :param emojis: Whether emojis should be kept
    :param urls: Whether urls should be kept
    :param hashtags: Whether hashtags should be kept
    :param mentions: Whether mentions should be kept
    :return: a list of tweets
    """
    if not data or "data" not in data:
        return []
    tweets = [tweet["text"] for tweet in data["data"]]
    if not mentions:
        tweets = [
            " ".join(word for word in tweet.split() if not word.startswith("@")) for tweet in tweets
        ]
    if not hashtags:
        tweets = [
            " ".join(word for word in tweet.split() if not word.startswith("#")) for tweet in tweets
        ]
    if not urls:
        tweets = [
            " ".join(word for word in tweet.split() if not word.startswith("https://t.co/"))
            for tweet in tweets
        ]
    if not emojis:
        # Does not detect emojis affixed to words without space but good enough for a first version
        tweets = [
            " ".join(w.decode() for w in tweet.encode("utf-8").split() if not w.startswith(b'\xF0'))
            for tweet in tweets
        ]
    return tweets
