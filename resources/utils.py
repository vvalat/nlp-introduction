import os
import sys
import re

from typing import List, Set

# From https://github.com/stopwords-iso/ with some punctuation and words additions
STOP_WORDS_SOURCE = "resources/stopwords-fr"


def remove_french_stop_words(input_words: List[str]) -> List[str]:
    with open(os.path.join(os.path.dirname(__file__), "..", STOP_WORDS_SOURCE), "r") as sw:
        stop_words = sw.read().splitlines()
        return [w for w in input_words if w.lower() not in stop_words]


def import_file(file_ref: str) -> str:
    file_name = None
    try:
        file_name = os.path.join(os.getcwd(), file_ref)
        if not os.path.isfile(file_name):
            print(f"Could not find '{file_name}' in '{os.getcwd()}'")
    except ValueError:
        print("Missing file name argument to proceed")
    if not file_name:
        exit(1)
    with open(file_name, "r") as file:
        return file.read()


def get_cli_args() -> List[str]:
    """
    Error handling method to retrieve arguments passed when invoking a script
    :return: a list of the arguments with the script name removed
    """
    try:
        _script, *terms = sys.argv
    except ValueError:
        terms = []
    return terms or []


def get_words_from_whitaker_dictionary(force_update: bool = False) -> Set[str]:
    """
    Get a unique list of words from the William Whitaker Latin-English dictionary
        source: http://archives.nd.edu/whitaker/dictpage.htm
    Note: line pattern: #(word), (variant)... POS ~spaces for tabulation~ [CODE] :: translation
    Note: the above pattern (mostly variants) is sometimes complex so it will miss some lines
    :param force_update: reprocess the dictionary even if the save file exists
    :return: a set of latin words (saved in a file if already processed)
    """
    def sanitize(input_string: str) -> List[str]:
        """
        Method that yields words after they have been sanitized (a lot of operations, unfortunately)
        :param input_string: result from the regex match group
        """
        for word in input_string.split(", "):
            for undesired in ("#", " (gen.)"):
                if undesired in word:
                    word = word.replace(undesired, "")
            if "  " in word:
                word, *_ = word.split(" ")
            if "(n)" in word:
                word = word.replace("(n)", "n")
            if word.endswith(" sum") or word.endswith(" est"):
                word = word[:-4]
            if word.endswith("i)"):
                if word.endswith("(ii)"):
                    word = word.replace("(ii)", "i")
                else:
                    word = word.replace("(i)", "i")
                yield word[:-1]
            if " (gen -ius)" in word:
                word = word.replace(" (gen -ius)", " -ius")
            if "-" in word:
                declension_2 = (
                    ("us", "a", "um"),
                    ("i", "ae", "a"),
                    ("or", "or", "us"),
                    ("us", "a", "u"),
                    ("es", "es", "ia"),
                    ("o", "ae", "o"),
                    ("or", "or", "u")
                )
                for suffix, dec1, dec2 in declension_2:
                    declension_match = f"{suffix} -{dec1} -{dec2}"
                    if word.endswith(declension_match):
                        stem = word.replace(declension_match, "")
                        word = f"{stem}{suffix}"
                        yield f"{stem}{dec1}"
                        yield f"{stem}{dec2}"
                        break
                declension_1 = (
                    ("a", "um"),
                    ("is", "e"),
                    ("um", "ius"),
                    ("us", "a"),
                )
                for suffix, dec in declension_1:
                    declension_match = f"{suffix} -{dec}"
                    if word.endswith(declension_match):
                        stem = word.replace(declension_match, "")
                        word = f"{stem}{suffix}"
                        yield f"{stem}{dec}"
                        break
            yield word

    current = os.path.dirname(__file__)
    save_path = os.path.join(current, "..", "out")
    if not os.path.exists(save_path):
        os.mkdir(save_path)

    save_file = os.path.join(save_path, "raw_latin_words")
    if os.path.isfile(save_file):
        with open(save_file, "r") as raw_words:
            return set(raw_words.read().splitlines())

    file_path = os.path.join(os.path.dirname(__file__), "whitaker_latin_dictionary")
    if not os.path.isfile(file_path):
        raise Exception("Missing latin dictionary")
    pattern = re.compile(r"^.*(?=\s{2}[A-Z])")
    words = []
    with open(file_path, "r") as dictionary:
        for line in dictionary.readlines():
            match = re.match(pattern, line)
            if match:
                words.extend(w for w in sanitize(match.group(0)))
            else:
                print(line)

    with open(save_file, "w") as raw_words:
        # Known artifacts post-processing and adding basic verb conjugations
        output = set(words).difference(("-", "undeclined", "(gen.)", "abb.")).union(
            "sum", "es", "est", "summus", "estis", "sunt", "habeo", "habes", "habet",
            "habemus", "habētis", "habent", "volo", "vis", "possum", "potes", "potest"
        )
        raw_words.write("\n".join(output))
    return output
