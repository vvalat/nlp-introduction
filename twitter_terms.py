"""
Display a html page containing word clouds for the search terms provided when invoking the script
Designed for French tweets, but only ned to load a different NLP server and removing lang argument
    in the `search_tweets` method for English
Note: could also easily display other POS's by editing/adding a new list comp' with the wanted tag
"""
import os
import webbrowser

from nltk import CoreNLPParser
from wordcloud import WordCloud
from matplotlib import pyplot

from resources.twitter import search_tweets, get_tweets_list
from resources.utils import get_cli_args

DIMENSIONS = {"width": 800, "height": 600}

if __name__ == "__main__":
    # Be sure to run `resources/run_stanford_core_nlp_server.sh` first
    tagger = CoreNLPParser("http://localhost:9004", tagtype="pos")
    if not get_cli_args():
        print("Missing query terms")
        exit(1)
    query_terms = " ".join(get_cli_args())
    tweets = get_tweets_list(
        search_tweets(query_terms, lang="fr", max_results=50, verified=True).json(), hashtags=True
    )

    save_path = os.path.join(os.getcwd(), "out")
    if not os.path.exists(save_path):
        os.mkdir(save_path)

    adjectives = [
        word for tweet in tweets for (word, tag) in tagger.tag(tweet.split())
        if tag == "ADJ" and not word.startswith("#")
    ]
    cloud = WordCloud(**DIMENSIONS, max_words=20).generate(" ".join(adjectives))
    pyplot.imshow(cloud)
    pyplot.axis("off")
    pyplot.savefig(os.path.join(save_path, "adj.png"))

    hashtags = [term for tweet in tweets for term in tweet.split() if term.startswith("#")]
    cloud = WordCloud(**DIMENSIONS, max_words=10).generate(" ".join(hashtags))
    pyplot.imshow(cloud)
    pyplot.axis("off")
    pyplot.savefig(os.path.join(save_path, "hash.png"))

    html = ("<!DOCTYPE html><html><head><title>Results for {query}</title></head><body>"
            "</body><figure><img src='{src_1}'><figcaption>Adjectives for “{query}” search"
            "</figcaption></figure><p><figure><img src='{src_2}'><figcaption>Hashtags for "
            "“{query}” search</figcaption></figure></html>")
    html_page = os.path.join(save_path, "twitter_terms.html")
    with open(html_page, "w") as html_file:
        html_file.write(html.format(query=query_terms, src_1="adj.png", src_2="hash.png"))
    webbrowser.open_new_tab(html_page)
