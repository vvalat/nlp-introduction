"""
Simple script to display the most frequent words in a French text file
Requirements: Tkinter, matplotlib, numpy
"""
from nltk import Text, word_tokenize, FreqDist
from resources.utils import get_cli_args, import_file, remove_french_stop_words

WORD_COUNT = 10


if __name__ == "__main__":
    if not get_cli_args():
        print("Missing file name")
        exit(1)
    input_words = word_tokenize(import_file(get_cli_args()[0]))
    output_words = remove_french_stop_words(input_words)
    print(
        # Just to illustrate the preferred way of getting a list of unique words by using a set
        f"Lexical diversity: {len(input_words) / len(set([w.lower() for w in output_words])):.2f}"
    )
    # Not needed to cast tokens as a nltk Text vs using the list as is, but allows for more options
    tokens = Text(output_words)
    tokens.dispersion_plot([word for word, _count in FreqDist(tokens).most_common(WORD_COUNT)])
