"""
Using dictionary references (static), this script aims to detect latin locutions in a different
language.
For now, success is around 50% with very few false positives. Improvement at this point would
require a Latin stemmer to detect declensions
"""

from collections import deque

from nltk import sent_tokenize, word_tokenize, UnigramTagger, DefaultTagger, trigrams

from resources.utils import get_words_from_whitaker_dictionary, get_cli_args, import_file


# For short words, there is a higher probability that the latin words exists in the target language
DISAMBIGUATION_THRESHOLD = 3


if __name__ == "__main__":
    if not get_cli_args():
        print("Missing reference for the text to tag")
        exit(1)
    sentences = []
    for chunk in import_file(get_cli_args()[0]).splitlines():
        sentences.extend(sent_tokenize(chunk))
    raw_latin_words = get_words_from_whitaker_dictionary()
    latin_words = dict((w, "LAT") for w in raw_latin_words if len(w) > DISAMBIGUATION_THRESHOLD)
    latin_words.update(
        dict((w, "?LAT") for w in raw_latin_words if len(w) <= DISAMBIGUATION_THRESHOLD)
    )
    latin_tagger = UnigramTagger(model=latin_words, backoff=DefaultTagger("OTHER"))
    locutions = []
    for sentence in sentences:
        parts = deque(latin_tagger.tag(word_tokenize(sentence)))
        try:
            while parts[0][1] == "OTHER":
                parts.popleft()
            locution = []
            while parts[0][1].endswith("LAT"):
                locution.append(parts.popleft()[0])
            if len(locution) >= 2:
                locutions.append(" ".join(locution))
        except IndexError:
            continue
    print(len(locutions), locutions)
