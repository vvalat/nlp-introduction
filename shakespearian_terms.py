"""
Using NLTK provided corpus, we illustrate the syntax of Conditional frequency distribution to
compare different events (words occurence) against different conditions (corpus)
Example use : `python shakespearian_terms.py death folly reason`
"""
from nltk import ConditionalFreqDist
from nltk.corpus import gutenberg

from resources.utils import get_cli_args

if __name__ == "__main__":
    if not get_cli_args():
        print("Missing query terms")
        exit(1)
    works = {
        "McBeth": gutenberg.words("shakespeare-macbeth.txt"),
        "Hamlet": gutenberg.words("shakespeare-hamlet.txt")
    }
    shakespeare_fd = ConditionalFreqDist(
        (work, word)    # (condition, event)
        for work in works.keys()
        for word in works[work]
    )
    shakespeare_fd.tabulate(samples=get_cli_args())    # It is also possible to limit conditions
