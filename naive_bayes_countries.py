"""
Run-of-the-mill test of Naive Bayes training to guess a belonging country from a city's name
Uses a 26K lines source, should be more precise with more complete non-free versions (1~4 Mil.)
Source: https://simplemaps.com/data/world-cities
Note: The list is heavily biased towards the United State which has a third of the entries
"""
import os
import sys
import random
from csv import DictReader

from nltk import NaiveBayesClassifier


SOURCE_FILE_PATH = os.path.join(os.getcwd(), "resources", "world_cities.csv")
VOWELS = ("a", "e", "i", "o", "u", "y")


def define_features(city_name: str) -> dict:
    """
    A feature list for nltk Naive Bayes is a dictionary with the name of the feature as key and the
    feature (derived information about the input: slice, count, or any objective information)
    Note: having too many features is called `overfitting`. Allocating a subset of the data to do
        error analysis (classify "guess" vs. actual label) or using nltk.classify.accuracy with
        different computations might help. This also heavily depends on the dataset itself.
    :param city_name: The city's name
    :return: features data structure
    """
    sanitized_name = " ".join([letter for letter in city_name if letter.isalpha()])
    char_count = len(city_name)
    vowel_count = len(
        [letter for letter in sanitized_name.replace(" ", "") if letter.lower() in VOWELS]
    )
    first_letter = city_name[0].lower()
    output = {
        "word_count": len(city_name.split()),
        "char_count": char_count,
        "vowel_count": vowel_count,
        "vowel_ratio": int(vowel_count / char_count * 100),
        "first_letter": first_letter,
        "first_letter_vowel": first_letter in VOWELS,
        "last_letter": city_name[-1]
    }
    for count_target in range(2, 5):
        if len(city_name.split()[-1]) > count_target:
            output[f"last_{count_target}_letters"] = "".join(
                [letter.lower() for letter in city_name[-count_target:]]
            )
    return output


if __name__ == "__main__":
    if not os.path.isfile(SOURCE_FILE_PATH):
        print("Missing cities source file")
        exit(1)
    if len(sys.argv) != 2:
        print("Bad argument format, invoke the script with a single city name in quotes")
        exit(1)
    _, city_query = sys.argv
    with open(SOURCE_FILE_PATH, "r") as csv_file:
        reader = DictReader(csv_file)
        cities = [(row["city"], row["country"]) for row in reader]
    random.shuffle(cities)    # Input file does name ordering but let's randomize anyway
    # The performance of the following line could be improved by using nltk.classify.apply_features
    # i.e. training_set = apply_features(define_features, cities[half_set:])
    feature_set = [(define_features(city), country) for city, country in cities]
    half_set = int(len(feature_set) / 2)
    training_set, testing_set = feature_set[half_set:], feature_set[:(len(feature_set) - half_set)]
    classifier = NaiveBayesClassifier.train(training_set)
    classifier.show_most_informative_features(10)
    print(classifier.classify(define_features(city_query)))
